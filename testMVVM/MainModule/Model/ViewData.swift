//
//  ViewData.swift
//  testMVVM
//
//  Created by roman on 7/16/21.
//  Copyright © 2021 roman. All rights reserved.
//

import Foundation

// view and format of DATA for our View
enum ViewData {
    case initial
    case loading(Data)
    case success(Data)
    case failure(Data)
    
    struct Data {
        let icon: String?
        let title: String?
        let description: String?
        let numberPhone: String?
    }
}
