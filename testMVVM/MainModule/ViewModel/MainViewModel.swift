//
//  MainViewModel.swift
//  testMVVM
//
//  Created by roman on 7/16/21.
//  Copyright © 2021 roman. All rights reserved.
//

import Foundation

protocol MainViewModelProtocol {
    var updateViewData: ((ViewData)->())? { get set }
    func startFetch()
    func failure()
}

final class MainViewModel: MainViewModelProtocol {
    public var updateViewData: ((ViewData) -> ())?
    
    init() {
        updateViewData?(.initial)
    }
    
    public func failure() {
        updateViewData?(.failure(ViewData.Data(icon: "failure",
                                                     title: "Error",
                                                     description: "NotUser",
                                                     numberPhone: nil)))
    }
    
    public func startFetch() {
        updateViewData?(.success(ViewData.Data(icon: "success",
                                                     title: "Success",
                                                     description: "Good",
                                                     numberPhone: nil)))
    }
}
